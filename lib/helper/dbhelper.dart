import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:toko_online/models/favorite.dart';
import 'package:toko_online/models/keranjang.dart';

class DbHelper {
  static DbHelper _dbHelper = DbHelper._createObject();
  //static Database? _database;

  DbHelper._createObject();

  factory DbHelper() {
    if (_dbHelper == null) {
      _dbHelper = DbHelper._createObject();
    }
    return _dbHelper;
  }
  Future<Database> get database async => await initDb();

  Future<Database> initDb() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'dicoatingsekalinew.db';

    var todoDatabase = openDatabase(path, version: 1, onCreate: _createDb);

    return todoDatabase;
  }

  //buat tabel baru
  void _createDb(Database db, int version) async {
    await db.execute('''
      CREATE TABLE keranjang (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        idproduk INTEGER,
        judul TEXT,
        harga TEXT,
        hargax TEXT,
        thumbnail TEXT,
        jumlah INTEGER,
        userid TEXT
      );  
    ''');
    await db.execute('''
      CREATE TABLE favorite (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        idproduk INTEGER,
        judul TEXT,
        harga TEXT,
        hargax TEXT,
        thumbnail TEXT
      );  
    ''');
  }

  Future<List<Map<String, dynamic>>> selectkeranjang() async {
    Database db = await this.database;
    var mapList = await db.rawQuery(
        'SELECT *, SUM(jumlah) as jumlah FROM keranjang GROUP BY idproduk');
    return mapList;
  }

  Future<List<Keranjang>> getkeranjang() async {
    var mapList = await selectkeranjang();
    int count = mapList.length;
    List<Keranjang> list = [];
    for (int i = 0; i < count; i++) {
      list.add(Keranjang.fromMap(mapList[i]));
    }
    return list;
  }

  Future<List<Map<String, dynamic>>> selectfavorite() async {
    Database db = await this.database;
    var mapList = await db.rawQuery('SELECT * FROM favorite');
    return mapList;
  }

  Future<List<Favorite>> getfavorite() async {
    var mapList = await selectfavorite();
    int count = mapList.length;
    List<Favorite> list = [];
    for (int i = 0; i < count; i++) {
      list.add(Favorite.fromMap(mapList[i]));
    }
    return list;
  }
}
