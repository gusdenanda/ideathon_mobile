class Toko1 {
  final int id;
  final String nama;

  Toko1({required this.id, required this.nama});

  factory Toko1.fromJson(Map<String, dynamic> json) {
    return Toko1(id: json['id'] as int, nama: json['store_nama'] as String);
  }
}
