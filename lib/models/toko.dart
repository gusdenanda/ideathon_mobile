class Toko {
  final int id;
  final String nama;
  final String nohp;
  final String alamat;

  Toko(
      {required this.id,
      required this.nama,
      required this.nohp,
      required this.alamat});

  factory Toko.fromJson(Map<String, dynamic> json) {
    return Toko(
      id: json['id'] as int,
      nama: json['store_nama'] as String,
      nohp: json['store_no_hp'] as String,
      alamat: json['store_alamat'] as String,
    );
  }
}
