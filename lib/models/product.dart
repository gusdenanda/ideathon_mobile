class Produk {
  final int id;
  final String thubmnail;
  final String judul;
  final int harga;
  final int stok;
  final String hargax;
  final String deskripsi;

  Produk(
      {required this.thubmnail,
      required this.id,
      required this.judul,
      required this.harga,
      required this.stok,
      required this.hargax,
      required this.deskripsi});

  factory Produk.fromJson(Map<String, dynamic> json) {
    return Produk(
        thubmnail: json['produk_gambar'] as String,
        id: json['id'] as int,
        judul: json['produk_nama'] as String,
        harga: json['produk_harga'] as int,
        stok: json['produk_stok'] as int,
        hargax: json['produk_hargax'] as String,
        deskripsi: json['produk_deskripsi'] as String);
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['thubmnail'] = thubmnail;
    map['id'] = id;
    map['judul'] = judul;
    map['harga'] = harga;
    map['stok'] = stok;
    map['hargax'] = hargax;
    map['deskripsi'] = deskripsi;
    return map;
  }
}
