class Keranjang {
  final int id;
  final String judul;
  final String harga;
  final String hargax;
  final int jumlah;
  final String userid;

  Keranjang(
      {required this.id,
      required this.judul,
      required this.harga,
      required this.hargax,
      required this.jumlah,
      required this.userid});

  factory Keranjang.fromJson(Map<String, dynamic> json) {
    return Keranjang(
        id: json['id'] as int,
        judul: json['produk_nama'] as String,
        harga: json['produk_harga'] as String,
        hargax: json['produk_hargax'] as String,
        jumlah: json['produk_jumlah'] as int,
        userid: json['userid'] as String);
  }

  factory Keranjang.fromMap(Map<String, dynamic> map) {
    return Keranjang(
        id: map['id'] as int,
        judul: map['judul'] as String,
        harga: map['harga'] as String,
        hargax: map['hargax'] as String,
        jumlah: map['jumlah'] as int,
        userid: map['userid'] as String);
  }
}
