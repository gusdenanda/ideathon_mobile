class Favorite {
  final int id;
  final String judul;
  final String harga;
  final String hargax;

  Favorite(
      {required this.id,
      required this.judul,
      required this.harga,
      required this.hargax});

  factory Favorite.fromJson(Map<String, dynamic> json) {
    return Favorite(
        id: json['id'] as int,
        judul: json['produk_nama'] as String,
        harga: json['produk_harga'] as String,
        hargax: json['produk_hargax'] as String);
  }

  factory Favorite.fromMap(Map<String, dynamic> map) {
    return Favorite(
        id: map['id'] as int,
        judul: map['judul'] as String,
        harga: map['harga'] as String,
        hargax: map['hargax'] as String);
  }
}
