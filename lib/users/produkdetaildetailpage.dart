import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:toko_online/constants.dart';
import 'package:toko_online/helper/dbhelper.dart';
import 'package:toko_online/models/favorite.dart';
import 'package:toko_online/models/keranjang.dart';
import 'package:toko_online/models/product.dart';
import 'package:toko_online/models/toko.dart';

import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ProdukDetailPage extends StatefulWidget {
  final Widget? child;
  final int id;
  final String judul;
  final int harga;
  final String hargax;
  final String thumbnail;
  final int stok;
  final String deskripsi;

  ProdukDetailPage(this.id, this.judul, this.harga, this.hargax, this.thumbnail,
      this.stok, this.deskripsi,
      {Key? key, this.child})
      : super(key: key);
  @override
  _ProdukDetailPageState createState() => _ProdukDetailPageState();
}

class _ProdukDetailPageState extends State<ProdukDetailPage> {
  int _counter = 0;
  List<Toko> tokolist = [];
  String _valcabang = "";
  bool instok = false;
  String userid = '';
  DbHelper dbHelper = DbHelper();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //fetchCabang();
    if (widget.stok > 0) {
      instok = true;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  /*Future<List<Toko>> fetchCabang() async {
    List<Toko> usersList = [];
    var params = "/api/store";
    try {
      var jsonResponse = await http.get(Uri.parse(Palette.sUrl + params));
      //print(json.decode(jsonResponse.body));
      if (jsonResponse.statusCode == 200) {
        final jsonItems =
            json.decode(jsonResponse.body).cast<Map<String, dynamic>>();
        usersList = jsonItems.map<Toko>((json) {
          return Toko.fromJson(json);
        }).toList();

        setState(() {
          tokolist = usersList;
        });
      }
    } catch (e) {
      usersList = tokolist;
    }
    return usersList;
  }*/

  saveKeranjang(Keranjang _keranjang) async {
    Database db = await dbHelper.database;
    var batch = db.batch();
    db.execute(
        'insert into keranjang(idproduk,judul,harga,hargax,thumbnail,jumlah,userid) values(?,?,?,?,?,?,?)',
        [
          _keranjang.id,
          _keranjang.judul,
          _keranjang.harga,
          _keranjang.hargax,
          '',
          _keranjang.jumlah,
          _keranjang.userid
        ]);
    await batch.commit();
    Navigator.of(context).pushNamedAndRemoveUntil(
        '/keranjangusers', (Route<dynamic> route) => false);
  }

  saveFavorite(Favorite _favorite) async {
    Database db = await dbHelper.database;
    var batch = db.batch();
    db.execute('delete from favorite where id=?', [_favorite.id]);
    db.execute(
        'insert into favorite(idproduk,judul,harga,hargax,thumbnail) values(?,?,?,?,?)',
        [
          _favorite.id,
          _favorite.judul,
          _favorite.harga,
          _favorite.hargax,
          '',
        ]);
    await batch.commit();
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/favorite', (Route<dynamic> route) => false);
  }

  Widget _body() {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            child: Image.asset('assets/images/logo-splash.png'),
          ),
          Padding(
            padding: EdgeInsets.only(top: 5.0),
            child: Text(widget.judul),
          ),
          Padding(
            padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
            child: Text(widget.deskripsi),
          ),
          Padding(
              padding: EdgeInsets.only(top: 15.0, bottom: 5.0),
              child: Text(
                widget.hargax,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                ),
              )),
          Padding(
            padding: EdgeInsets.only(left: 0, top: 15.0, bottom: 5.0),
            child: Container(
              child: Text(
                'Rekomendasi Produk Terkait',
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.center,
              ),
              width: 400.0,
              padding:
                  EdgeInsets.only(bottom: 2.0, right: 10.0, top: 2.0, left: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0),
                  topLeft: Radius.circular(10.0),
                  bottomLeft: Radius.circular(10.0),
                ),
                color: Palette.colorrekom,
                boxShadow: [
                  BoxShadow(color: Palette.colorrekom, spreadRadius: 1),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<Produk> produklist = [];

  Future<List<Produk>> fetchProduk(String id) async {
    List<Produk> usersList;
    var params = "/api/produkbytoko/1";

    try {
      var jsonResponse = await http.get(Uri.parse(Palette.sUrl + params));
      if (jsonResponse.statusCode == 200) {
        final jsonItems =
            json.decode(jsonResponse.body).cast<Map<String, dynamic>>();

        usersList = jsonItems.map<Produk>((json) {
          return Produk.fromJson(json);
        }).toList();
        //print(usersList);
        @override
        void initState() {
          produklist = usersList;
          super.initState();
        }
      } else {
        usersList = produklist;
      }
    } catch (e) {
      usersList = produklist;
    }
    //print(usersList);
    return usersList;
  }

  Widget _rekomendasi() {
    return Container(
      height: 200.0,
      margin: EdgeInsets.only(bottom: 7.0),
      child: FutureBuilder<List<Produk>>(
        future: fetchProduk(widget.id.toString()),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(child: CircularProgressIndicator());
          return ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: snapshot.data!.length,
            itemBuilder: (BuildContext context, int i) => Card(
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute<Null>(builder: (BuildContext context) {
                    return new ProdukDetailPage(
                        snapshot.data![i].id,
                        snapshot.data![i].judul,
                        snapshot.data![i].harga,
                        snapshot.data![i].hargax,
                        snapshot.data![i].thubmnail,
                        snapshot.data![i].stok,
                        snapshot.data![i].deskripsi);
                  }));
                },
                child: Container(
                  width: 135.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Image.asset('assets/images/logo-splash.png'),
                      Padding(
                        padding: EdgeInsets.only(top: 5.0),
                        child: Text(
                          snapshot.data![i].judul,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                        child: Text(snapshot.data![i].hargax),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Produk'),
        backgroundColor: Palette.bg1,
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _body(),
                _rekomendasi(),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        child: Container(
          child: Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      Favorite _favoritku = Favorite(
                          id: widget.id,
                          judul: widget.judul,
                          harga: widget.harga.toString(),
                          hargax: widget.hargax);
                      saveFavorite(_favoritku);
                    },
                    child: Container(
                      child: Icon(
                        Icons.favorite_border,
                        size: 40.0,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.grey[100],
                        boxShadow: [
                          BoxShadow(color: Colors.grey, spreadRadius: 1),
                        ],
                      ),
                    ),
                  )),
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/keranjangusers', (Route<dynamic> route) => false);
                    },
                    child: Container(
                      child: Icon(
                        Icons.shopping_cart,
                        size: 40.0,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.grey[100],
                        boxShadow: [
                          BoxShadow(color: Colors.grey, spreadRadius: 1),
                        ],
                      ),
                    ),
                  )),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    if (instok == true) {
                      Keranjang _keranjangku = Keranjang(
                          id: widget.id,
                          judul: widget.judul,
                          harga: widget.harga.toString(),
                          hargax: widget.hargax,
                          jumlah: 1,
                          userid: userid);
                      saveKeranjang(_keranjangku);
                    }
                  },
                  child: Container(
                    height: 40.0,
                    child: Center(
                      child: Text('Beli Sekarang',
                          style: TextStyle(color: Colors.white)),
                    ),
                    decoration: BoxDecoration(
                      color: instok == true ? Colors.blue : Colors.grey,
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: [
                        BoxShadow(
                            color: instok == true ? Colors.blue : Colors.grey,
                            spreadRadius: 1),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          height: 60.0,
          padding:
              EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0, bottom: 2.0),
          decoration: BoxDecoration(
            color: Colors.grey[100],
            boxShadow: [
              BoxShadow(color: Colors.grey, spreadRadius: 1),
            ],
          ),
        ),
        elevation: 0,
      ),
    );
  }
}
