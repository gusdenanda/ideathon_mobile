import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:toko_online/constants.dart';
import 'package:toko_online/login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:toko_online/helper/dbhelper.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController namaController = TextEditingController();
  final TextEditingController nohpController = TextEditingController();
  final TextEditingController alamatController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool visible = false;
  DbHelper dbHelper = DbHelper();

  @override
  void initState() {
    super.initState();
  }

  _updateKeranjang(String userid) async {
    Database db = await dbHelper.database;
    var batch = db.batch();
    db.execute('update keranjang set userid=?', [userid]);
    batch.commit();
  }

  loadingProses(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  _register() async {
    // setState(() {
    //   visible = true;
    // });
    var params = "/api/register";
    var requestBody = {
      'email': emailController.text,
      'name': namaController.text,
      'nohp': nohpController.text,
      'alamat': alamatController.text,
      'password': passwordController.text
    };
    var res = '';
    //print(requestBody);
    // Navigator.of(context)
    //     .push(MaterialPageRoute(builder: (context) => LoginPage()));
    try {
      await http
          .post(Uri.parse(Palette.sUrl + params), body: requestBody)
          .then((response) {
        res = response.body.toString();
        // if (res == "OK") {
        //   // setState(() {
        //   //   visible = false;
        //   // });
        //   // Navigator.of(context).pushNamedAndRemoveUntil(
        //   //     '/login', (Route<dynamic> route) => false);
        //   Navigator.of(context)
        //       .push(MaterialPageRoute(builder: (context) => LoginPage()));
        // }
        _showAlertDialog(context, "OK");
      });
    } catch (e) {}

    return params;
  }

  _showAlertDialog(BuildContext context, String err) {
    if (err == 'OK') {
      Widget okButton = FlatButton(
        child: Text("OK"),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => LoginPage()));
        },
        // onPressed: () => LoginPage(),
      );
      AlertDialog alert = AlertDialog(
        content: Text("Pendaftaran Success"),
        actions: [
          okButton,
        ],
      );
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    } else {
      Widget okButton = FlatButton(
        child: Text("OK"),
        //onPressed: () {},
        onPressed: () => Navigator.pop(context),
      );
      AlertDialog alert = AlertDialog(
        title: Text("Error"),
        content: Text(err),
        actions: [
          okButton,
        ],
      );
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registrasi'),
        backgroundColor: Palette.bg1,
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _header(),
                Container(
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                      Visibility(
                          maintainSize: true,
                          maintainAnimation: true,
                          maintainState: true,
                          visible: visible,
                          child: Container(child: CircularProgressIndicator())),
                    ])),
                _body(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _header() {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(15.0, 30.0, 0.0, 0.0),
            child: Text('Dicoating',
                style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold)),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(16.0, 65.0, 0.0, 0.0),
            child: Text('sekali',
                style: TextStyle(fontSize: 60.0, fontWeight: FontWeight.bold)),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(170.0, 48.0, 0.0, 20.0),
            child: Text('.',
                style: TextStyle(
                    fontSize: 80.0,
                    fontWeight: FontWeight.bold,
                    color: Palette.menuNiaga)),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(15.0, 150.0, 0.0, 0.0),
            child: Text('Registrasi Pengguna Baru',
                style: TextStyle(fontSize: 20.0)),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(260.0, 28.0, 0.0, 20.0),
            child: Image.asset(
              'assets/images/logo-splash.png',
              width: 100,
              height: 100,
            ),
          ),
        ],
      ),
    );
  }

  Widget _body() {
    return Container(
        padding: EdgeInsets.only(top: 0.0, left: 20.0, right: 20.0),
        child: Column(
          children: <Widget>[
            TextField(
              controller: emailController,
              keyboardType: TextInputType.text,
              autocorrect: false,
              decoration: InputDecoration(
                  labelText: 'Email',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Palette.menuNiaga))),
            ),
            SizedBox(height: 20.0),
            TextField(
              controller: namaController,
              keyboardType: TextInputType.text,
              autocorrect: false,
              decoration: InputDecoration(
                  labelText: 'Nama',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Palette.menuNiaga))),
            ),
            SizedBox(height: 20.0),
            TextField(
              controller: nohpController,
              keyboardType: TextInputType.text,
              autocorrect: false,
              decoration: InputDecoration(
                  labelText: 'No. Hp',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Palette.menuNiaga))),
            ),
            SizedBox(height: 20.0),
            TextField(
              controller: alamatController,
              keyboardType: TextInputType.text,
              autocorrect: false,
              decoration: InputDecoration(
                  labelText: 'Alamat',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Palette.menuNiaga))),
            ),
            SizedBox(height: 20.0),
            TextField(
              controller: passwordController,
              keyboardType: TextInputType.text,
              autocorrect: false,
              decoration: InputDecoration(
                  labelText: 'Password',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Palette.menuNiaga))),
              obscureText: true,
            ),
            SizedBox(height: 40.0),
            InkWell(
              //onTap: _register(),
              onTap: () {
                _register(); // here you can also use async-await
              },
              // onTap: () {
              //   Navigator.of(context).pushNamedAndRemoveUntil(
              //       '/dashboard', (Route<dynamic> route) => false);
              // },
              child: Container(
                height: 60.0,
                child: Material(
                  borderRadius: BorderRadius.circular(10.0),
                  shadowColor: Colors.blue[800],
                  color: Palette.menuNiaga,
                  elevation: 7.0,
                  child: Center(
                    child: Text(
                      'Register',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat'),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 15.0),
          ],
        ));
  }
}
