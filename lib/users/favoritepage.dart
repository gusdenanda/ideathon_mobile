import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:toko_online/constants.dart';
import 'package:toko_online/helper/dbhelper.dart';
import 'package:toko_online/login.dart';
import 'package:toko_online/models/favorite.dart';
import 'package:toko_online/users/payment.dart';

class FavoritePage extends StatefulWidget {
  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  int _counter = 0;
  DbHelper dbHelper = DbHelper();
  List<Favorite> favoritelist = [];
  int _subTotal = 0;
  bool login = false;
  String userid = '';

  @override
  void initState() {
    super.initState();
    getfavorite();
  }

  Future<List<Favorite>> getfavorite() async {
    final Future<Database> dbFuture = dbHelper.initDb();
    dbFuture.then((database) {
      Future<List<Favorite>> listFuture = dbHelper.getfavorite();
      listFuture.then((_favoritelist) {
        if (mounted) {
          setState(() {
            favoritelist = _favoritelist;
          });
        }
      });
    });
    return favoritelist;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Favorite'),
        backgroundColor: Palette.bg1,
      ),
      body: favoritelist.isEmpty ? _favoriteKosong() : _widgetFavorite(),
    );
  }

  _deleteFavorite(int id) async {
    Database db = await dbHelper.database;
    var batch = db.batch();
    db.execute('delete from favorite where id=?', [id]);
    await batch.commit();
  }

  _kosongkanFavorite() async {
    Database db = await dbHelper.database;
    var batch = db.batch();
    db.execute('delete from favorite');
    await batch.commit();
  }

  Widget _favoriteKosong() {
    return FutureBuilder(
      future: Future.delayed(Duration(seconds: 1)),
      builder: (c, s) => s.connectionState == ConnectionState.done
          ? favoritelist.isEmpty
              ? SafeArea(
                  child: new Container(
                    color: Colors.white,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: Center(
                            child: Container(
                                padding:
                                    EdgeInsets.only(left: 25.0, right: 25.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Favorite Kosong',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                  ],
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : Center(
                  child: CircularProgressIndicator(),
                )
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }

  Widget _widgetFavorite() {
    return SafeArea(
      child: new Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Expanded(
              child: FutureBuilder<List<Favorite>>(
                future: getfavorite(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(child: CircularProgressIndicator());

                  return ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, i) {
                      return Container(
                        height: 110.0,
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: Colors.grey,
                              width: 1.0,
                            ),
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.white, spreadRadius: 1),
                          ],
                        ),
                        child: ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(
                              left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
                          title: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Image.asset('assets/images/logo-splash.png'),
                                Expanded(
                                  child: Container(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(snapshot.data![i].judul,
                                            style: TextStyle(fontSize: 16.0)),
                                        Text(snapshot.data![i].hargax,
                                            style: TextStyle(
                                                color: Colors.red,
                                                fontSize: 14.0)),
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(top: 10.0),
                                                padding: EdgeInsets.only(
                                                    right: 10.0,
                                                    top: 7.0,
                                                    bottom: 5.0),
                                                child: Align(
                                                  alignment:
                                                      Alignment.centerRight,
                                                  child: InkWell(
                                                    onTap: () {
                                                      _deleteFavorite(
                                                          snapshot.data![i].id);
                                                    },
                                                    child: Container(
                                                      height: 25,
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(2),
                                                        border: Border.all(
                                                            color: Colors.red),
                                                        boxShadow: [
                                                          BoxShadow(
                                                              color: Colors.red,
                                                              spreadRadius: 1),
                                                        ],
                                                      ),
                                                      child: Icon(
                                                        Icons.delete,
                                                        color: Colors.white,
                                                        size: 22,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {},
                        ),
                      );
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
