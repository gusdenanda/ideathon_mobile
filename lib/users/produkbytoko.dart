import 'package:flutter/material.dart';
import 'package:toko_online/constants.dart';
import 'package:toko_online/models/product.dart';
import 'package:toko_online/models/toko.dart';
import 'package:http/http.dart' as http;

import 'dart:async';
import 'dart:convert';

import 'package:toko_online/users/produkdetaildetailpage.dart';

class ProdukByToko extends StatefulWidget {
  final Widget? child;
  final int id;

  ProdukByToko(this.id, {Key? key, this.child}) : super(key: key);

  @override
  _ProdukByTokoState createState() => _ProdukByTokoState();
}

class _ProdukByTokoState extends State<ProdukByToko> {
  int _counter = 0;

  List<Produk> produklist = [];

  Future<List<Produk>> fetchProduk(String id) async {
    List<Produk> usersList;
    var params = "/api/produkbytoko/" + id;
    print(params);
    try {
      var jsonResponse = await http.get(Uri.parse(Palette.sUrl + params));
      if (jsonResponse.statusCode == 200) {
        final jsonItems =
            json.decode(jsonResponse.body).cast<Map<String, dynamic>>();

        usersList = jsonItems.map<Produk>((json) {
          return Produk.fromJson(json);
        }).toList();
        //print(usersList);
        @override
        void initState() {
          produklist = usersList;
          super.initState();
        }
      } else {
        usersList = produklist;
      }
    } catch (e) {
      usersList = produklist;
    }
    //print(usersList);
    return usersList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Produk'),
        backgroundColor: Palette.bg1,
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Expanded(
              child: FutureBuilder<List<Produk>>(
                future: fetchProduk(widget.id.toString()),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(child: CircularProgressIndicator());

                  return ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, i) {
                      return Container(
                        height: 110.0,
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: Colors.grey,
                              width: 1.0,
                            ),
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.white, spreadRadius: 1),
                          ],
                        ),
                        child: ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(
                              left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
                          title: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Image.asset('assets/images/logo-splash.png'),
                                Expanded(
                                  child: Container(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(snapshot.data![i].judul,
                                            style: TextStyle(fontSize: 16.0)),
                                        Text(snapshot.data![i].hargax,
                                            style: TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.w600,
                                            )),
                                        InkWell(
                                          onTap: () {
                                            Navigator.of(context).push(
                                                MaterialPageRoute<Null>(builder:
                                                    (BuildContext context) {
                                              return new ProdukDetailPage(
                                                  snapshot.data![i].id,
                                                  snapshot.data![i].judul,
                                                  snapshot.data![i].harga,
                                                  snapshot.data![i].hargax,
                                                  snapshot.data![i].thubmnail,
                                                  snapshot.data![i].stok,
                                                  snapshot.data![i].deskripsi);
                                            }));
                                          },
                                          child: Container(
                                            margin: const EdgeInsets.only(
                                                top: 10.0),
                                            height: 20.0,
                                            width: 120,
                                            child: Material(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              shadowColor: Colors.blue[800],
                                              color: Palette.menuNiaga,
                                              elevation: 7.0,
                                              child: Center(
                                                child: Text(
                                                  'Detail Produk',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontFamily: 'Montserrat'),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute<Null>(
                                builder: (BuildContext context) {
                              return new ProdukDetailPage(
                                  snapshot.data![i].id,
                                  snapshot.data![i].judul,
                                  snapshot.data![i].harga,
                                  snapshot.data![i].hargax,
                                  snapshot.data![i].thubmnail,
                                  snapshot.data![i].stok,
                                  snapshot.data![i].deskripsi);
                            }));
                          },
                        ),
                      );
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
