import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:toko_online/constants.dart';
import 'package:toko_online/helper/dbhelper.dart';
import 'package:toko_online/login.dart';
import 'package:toko_online/models/keranjang.dart';
import 'package:toko_online/users/payment.dart';

class PembelianPage extends StatefulWidget {
  @override
  _PembelianPageState createState() => _PembelianPageState();
}

class _PembelianPageState extends State<PembelianPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController namaController = TextEditingController();
  TextEditingController nohpController = TextEditingController();
  TextEditingController alamatController = TextEditingController();
  TextEditingController jumlahBayarController = TextEditingController();

  int _counter = 0;
  DbHelper dbHelper = DbHelper();
  List<Keranjang> keranjanglist = [];
  int _subTotal = 0;
  bool login = false;
  String userid = '';

  int _username = 0;
  bool _login = true;
  String _nama = '';
  String _email = '';
  String _foto = '';
  String _nohp = '';
  String _alamat = '';

  @override
  void initState() {
    super.initState();
    getkeranjang();
    _loadPref();
  }

  _loadPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _username = (prefs.getInt('_username') ?? 0);
      _nama = (prefs.getString('nama') ?? '');
      _email = (prefs.getString('email') ?? '');
      _foto = (prefs.getString('foto') ?? '');
      _nohp = (prefs.getString('nohp') ?? '');
      _alamat = (prefs.getString('alamat') ?? '');
      _login = (prefs.getBool('login') ?? false);

      namaController = new TextEditingController(text: _nama);
      nohpController = new TextEditingController(text: _nohp);
      alamatController = new TextEditingController(text: _alamat);
    });
  }

  Future<List<Keranjang>> getkeranjang() async {
    final Future<Database> dbFuture = dbHelper.initDb();
    dbFuture.then((database) {
      Future<List<Keranjang>> listFuture = dbHelper.getkeranjang();
      listFuture.then((_keranjanglist) {
        if (mounted) {
          setState(() {
            keranjanglist = _keranjanglist;
          });
        }
      });
    });
    int subtotal = 0;
    for (int i = 0; i < keranjanglist.length; i++) {
      if (keranjanglist[i].harga.trim() != "0") {
        subtotal +=
            keranjanglist[i].jumlah * int.parse(keranjanglist[i].harga.trim());
      }
    }
    setState(() {
      _subTotal = subtotal;
    });
    return keranjanglist;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pembelian'),
        backgroundColor: Palette.bg1,
      ),
      body: _widgetKeranjang(),
      bottomNavigationBar: Visibility(
        visible: keranjanglist.isEmpty ? false : true,
        child: BottomAppBar(
          color: Colors.transparent,
          child: Container(
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 5.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Total', style: TextStyle(fontSize: 14.0)),
                        Text('Rp. ' + _subTotal.toString(),
                            style:
                                TextStyle(color: Colors.red, fontSize: 18.0)),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      // Navigator.of(context).push(MaterialPageRoute<Null>(
                      //     builder: (BuildContext context) {
                      //   return new PaymentPage(_subTotal.toString());
                      // }));
                      Navigator.of(context).push(MaterialPageRoute<Null>(
                          builder: (BuildContext context) {
                        return new LoginPage();
                      }));
                      // login
                      //     ? _klikCekout(keranjanglist)
                      //     : Navigator.of(context).push(MaterialPageRoute<Null>(
                      //         builder: (BuildContext context) {
                      //         return new Login();
                      //       }));
                    },
                    child: Container(
                      height: 40.0,
                      child: Center(
                        child: Text('Submit',
                            style: TextStyle(color: Colors.white)),
                      ),
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(color: Colors.blue, spreadRadius: 1),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            height: 70.0,
            padding:
                EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0, bottom: 2.0),
            decoration: BoxDecoration(
              color: Colors.grey[100],
              boxShadow: [
                BoxShadow(color: Colors.grey, spreadRadius: 1),
              ],
            ),
          ),
          elevation: 0,
        ),
      ),
    );
  }

  _tambahJmlKeranjang(int id) async {
    Database db = await dbHelper.database;
    var batch = db.batch();
    db.execute('update keranjang set jumlah=jumlah+1 where id=?', [id]);
    await batch.commit();
  }

  _kurangJmlKeranjang(int id) async {
    Database db = await dbHelper.database;
    var batch = db.batch();
    db.execute('update keranjang set jumlah=jumlah-1 where id=?', [id]);
    await batch.commit();
  }

  _deleteKeranjang(int id) async {
    Database db = await dbHelper.database;
    var batch = db.batch();
    db.execute('delete from keranjang where id=?', [id]);
    await batch.commit();
  }

  _kosongkanKeranjang() async {
    Database db = await dbHelper.database;
    var batch = db.batch();
    db.execute('delete from keranjang');
    await batch.commit();
  }

  Widget _widgetForm() {
    return Container(
        padding: EdgeInsets.only(top: 0.0, left: 20.0, right: 20.0),
        child: Column(
          children: <Widget>[
            SizedBox(height: 20.0),
            TextField(
              controller: namaController,
              keyboardType: TextInputType.text,
              autocorrect: false,
              decoration: InputDecoration(
                  labelText: 'Nama',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Palette.menuNiaga))),
            ),
            SizedBox(height: 20.0),
            TextField(
              controller: nohpController,
              keyboardType: TextInputType.text,
              autocorrect: false,
              decoration: InputDecoration(
                  labelText: 'No. Hp',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Palette.menuNiaga))),
            ),
            SizedBox(height: 20.0),
            TextField(
              controller: alamatController,
              keyboardType: TextInputType.text,
              autocorrect: false,
              decoration: InputDecoration(
                  labelText: 'Alamat',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Palette.menuNiaga))),
            ),
            SizedBox(height: 20.0),
            TextField(
              controller: jumlahBayarController,
              keyboardType: TextInputType.text,
              autocorrect: false,
              decoration: InputDecoration(
                  labelText: 'Jumlah Bayar',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Palette.menuNiaga))),
            ),
            SizedBox(height: 40.0),
          ],
        ));
  }

  Widget _keranjangKosong() {
    return FutureBuilder(
      future: Future.delayed(Duration(seconds: 1)),
      builder: (c, s) => s.connectionState == ConnectionState.done
          ? keranjanglist.isEmpty
              ? SafeArea(
                  child: new Container(
                    color: Colors.white,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: Center(
                            child: Container(
                                padding:
                                    EdgeInsets.only(left: 25.0, right: 25.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Keranjang Kosong',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                  ],
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : Center(
                  child: CircularProgressIndicator(),
                )
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }

  Widget _widgetKeranjang() {
    return SafeArea(
      child: new Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Expanded(
              child: FutureBuilder<List<Keranjang>>(
                future: getkeranjang(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(child: CircularProgressIndicator());

                  return ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, i) {
                      return Container(
                        height: 110.0,
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: Colors.grey,
                              width: 1.0,
                            ),
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.white, spreadRadius: 1),
                          ],
                        ),
                        child: ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(
                              left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
                          title: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Image.asset('assets/images/logo-splash.png'),
                                Expanded(
                                  child: Container(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(snapshot.data![i].judul,
                                            style: TextStyle(fontSize: 16.0)),
                                        Text(snapshot.data![i].hargax,
                                            style: TextStyle(
                                                color: Colors.red,
                                                fontSize: 14.0)),
                                        Text(
                                            'Jumlah : ' +
                                                snapshot.data![i].jumlah
                                                    .toString(),
                                            style: TextStyle(fontSize: 14.0)),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {},
                        ),
                      );
                    },
                  );
                },
              ),
            ),
            _widgetForm(),
          ],
        ),
      ),
    );
  }
}
