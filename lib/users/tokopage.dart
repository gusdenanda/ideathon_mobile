import 'package:flutter/material.dart';
import 'package:toko_online/constants.dart';
import 'package:toko_online/models/toko.dart';
import 'package:http/http.dart' as http;
import 'package:toko_online/users/produkbytoko.dart';

import 'dart:async';
import 'dart:convert';

import 'package:toko_online/users/produkdetaildetailpage.dart';

class TokoPage extends StatefulWidget {
  @override
  _TokoPageState createState() => _TokoPageState();
}

class _TokoPageState extends State<TokoPage> {
  int _counter = 0;
  List<Toko> tokolist = [];

  Future<List<Toko>> fetchToko() async {
    List<Toko> usersList = [];
    var params = "/api/store";
    try {
      var jsonResponse = await http.get(Uri.parse(Palette.sUrl + params));
      //print(json.decode(jsonResponse.body));
      if (jsonResponse.statusCode == 200) {
        final jsonItems =
            json.decode(jsonResponse.body).cast<Map<String, dynamic>>();
        usersList = jsonItems.map<Toko>((json) {
          return Toko.fromJson(json);
        }).toList();

        setState(() {
          tokolist = usersList;
        });
      }
    } catch (e) {
      usersList = tokolist;
    }
    return usersList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Expanded(
              child: FutureBuilder<List<Toko>>(
                future: fetchToko(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(child: CircularProgressIndicator());

                  return ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, i) {
                      return Container(
                        height: 110.0,
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: Colors.grey,
                              width: 1.0,
                            ),
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.white, spreadRadius: 1),
                          ],
                        ),
                        child: ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.only(
                              left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
                          title: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Image.asset('assets/images/logo-splash.png'),
                                Expanded(
                                  child: Container(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                            padding: EdgeInsets.fromLTRB(
                                                10, 5, 0, 5),
                                            child: Text(snapshot.data![i].nama,
                                                style:
                                                    TextStyle(fontSize: 16))),
                                        Container(
                                            padding: EdgeInsets.fromLTRB(
                                                10, 5, 0, 15),
                                            child: Text(
                                                'No. HP : ' +
                                                    snapshot.data![i].nohp,
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w600,
                                                ))),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute<Null>(
                                builder: (BuildContext context) {
                              return new ProdukByToko(snapshot.data![i].id);
                            }));
                          },
                        ),
                      );
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
