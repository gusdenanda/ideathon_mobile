import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toko_online/constants.dart';
import 'package:toko_online/models/product.dart';
import 'package:toko_online/models/toko.dart';
import 'dart:async';
import 'dart:convert';

import 'package:toko_online/users/produkdetaildetailpage.dart';

class DepanPage extends StatefulWidget {
  @override
  _DepanPageState createState() => _DepanPageState();
}

class _DepanPageState extends State<DepanPage> {
  List<Toko> tokolist = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchToko();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    tokolist = [];
  }

  Future<List<Toko>> fetchToko() async {
    List<Toko> usersList = [];
    var params = "/api/store";
    try {
      var jsonResponse = await http.get(Uri.parse(Palette.sUrl + params));
      //print(json.decode(jsonResponse.body));
      if (jsonResponse.statusCode == 200) {
        final jsonItems =
            json.decode(jsonResponse.body).cast<Map<String, dynamic>>();
        usersList = jsonItems.map<Toko>((json) {
          return Toko.fromJson(json);
        }).toList();

        setState(() {
          tokolist = usersList;
        });
      }
    } catch (e) {
      usersList = tokolist;
    }
    return usersList;
  }

  Future<Null> _refresh() {
    return fetchToko().then((_toko) {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[produkbyToko()],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget produkbyToko() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          for (int i = 0; i < tokolist.length; i++)
            WidgetbyToko(tokolist[i].id, tokolist[i].nama.toString(), i),
        ],
      ),
    );
  }
}

class WidgetbyToko extends StatefulWidget {
  final int id;
  final String toko;
  final int warna;

  const WidgetbyToko(this.id, this.toko, this.warna, {Key? key})
      : super(key: key);

  _WidgetbyTokoState createState() => _WidgetbyTokoState();
}

class _WidgetbyTokoState extends State<WidgetbyToko> {
  List<Produk> produklist = [];

  Future<List<Produk>> fetchProduk(String id) async {
    List<Produk> usersList;
    var params = "/api/produkbytoko/" + id;

    try {
      var jsonResponse = await http.get(Uri.parse(Palette.sUrl + params));
      if (jsonResponse.statusCode == 200) {
        final jsonItems =
            json.decode(jsonResponse.body).cast<Map<String, dynamic>>();

        usersList = jsonItems.map<Produk>((json) {
          return Produk.fromJson(json);
        }).toList();
        //print(usersList);

        @override
        void initState() {
          produklist = usersList;
          super.initState();
        }
      } else {
        usersList = produklist;
      }
    } catch (e) {
      usersList = produklist;
    }
    //print(usersList);
    return usersList;
  }

  @override
  Widget build(BuildContext context) {
    //fetchProduk('1');
    return Container(
      margin: EdgeInsets.only(bottom: 20.0),
      padding: EdgeInsets.only(right: 10.0),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
            padding: EdgeInsets.only(right: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    widget.toko,
                    style: TextStyle(color: Colors.white),
                  ),
                  width: 200.0,
                  padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0)),
                    color: Palette.colors[widget.warna],
                    boxShadow: [
                      BoxShadow(
                          color: Palette.colors[widget.warna], spreadRadius: 1),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    /* Navigator.of((context)).push(MaterialPageRoute<Null>(
                      builder: (BuildContext context){
                        return new ProdukPage(
                          "kat", widget.id, 0, widget.toko
                        );
                      }
                    ));*/
                  },
                  child: Text(
                    'Selengkapnya',
                    style: TextStyle(color: Colors.blue),
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 200.0,
            margin: EdgeInsets.only(bottom: 7.0),
            child: FutureBuilder<List<Produk>>(
              future: fetchProduk(widget.id.toString()),
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return Center(child: CircularProgressIndicator());
                return ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: snapshot.data!.length,
                  itemBuilder: (BuildContext context, int i) => Card(
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute<Null>(
                            builder: (BuildContext context) {
                          return new ProdukDetailPage(
                              snapshot.data![i].id,
                              snapshot.data![i].judul,
                              snapshot.data![i].harga,
                              snapshot.data![i].hargax,
                              snapshot.data![i].thubmnail,
                              snapshot.data![i].stok,
                              snapshot.data![i].deskripsi);
                        }));
                      },
                      child: Container(
                        width: 135.0,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Image.asset('assets/images/logo-splash.png'),
                            Padding(
                              padding: EdgeInsets.only(top: 5.0),
                              child: Text(
                                snapshot.data![i].judul,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                              child: Text(snapshot.data![i].hargax),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
