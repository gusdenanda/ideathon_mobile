import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toko_online/constants.dart';
import 'package:toko_online/login.dart';

class AkunPage extends StatefulWidget {
  @override
  _AkunPageState createState() => _AkunPageState();
}

class _AkunPageState extends State<AkunPage> {
  int _counter = 0;
  int _username = 0;
  bool _login = true;
  String _nama = '';
  String _email = '';
  String _foto = '';
  String _nohp = '';
  String _alamat = '';

  @override
  void initState() {
    super.initState();
    _loadPref();
  }

  _loadPref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _username = (prefs.getInt('_username') ?? 0);
      _nama = (prefs.getString('nama') ?? '');
      _email = (prefs.getString('email') ?? '');
      _foto = (prefs.getString('foto') ?? '');
      _nohp = (prefs.getString('nohp') ?? '');
      _alamat = (prefs.getString('alamat') ?? '');
      _login = (prefs.getBool('login') ?? false);
    });
  }

  Widget _profile() {
    return ListView(
      children: <Widget>[
        Container(
          height: 250,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.blue.shade900, Colors.blue.shade700],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              stops: [0.5, 0.9],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  CircleAvatar(
                    backgroundColor: Colors.red,
                    minRadius: 35.0,
                    child: Icon(
                      Icons.call,
                      size: 30.0,
                    ),
                  ),
                  CircleAvatar(
                    backgroundColor: Colors.white70,
                    minRadius: 60.0,
                    child: CircleAvatar(
                      radius: 50.0,
                      backgroundImage: NetworkImage(
                          'https://scontent.fdps2-1.fna.fbcdn.net/v/t1.6435-1/p200x200/97137652_3480870461925174_5850731843636166656_n.jpg?_nc_cat=107&ccb=1-5&_nc_sid=7206a8&_nc_eui2=AeHGcc5uKsiEQbjqQcZk6YKFAsi0WHrdyTwCyLRYet3JPNt2VR6UF6HumCm9NLmsX8k1hjdeKGlhk0OR7TdwQE9o&_nc_ohc=fJi2pubbNUEAX-cS-Zj&tn=PfMztkKp_FeA0Lgo&_nc_ht=scontent.fdps2-1.fna&oh=6fe8245a747597077256b11671b20f25&oe=6183C722'),
                    ),
                  ),
                  CircleAvatar(
                    backgroundColor: Colors.red,
                    minRadius: 35.0,
                    child: Icon(
                      Icons.message,
                      size: 30.0,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                _nama,
                style: TextStyle(
                  fontSize: 35,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
              Text(
                'Member',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                ),
              ),
            ],
          ),
        ),
        Container(
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  color: Colors.blue.shade700,
                  child: ListTile(
                    title: Text(
                      'Dicoating',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  color: Colors.blue.shade300,
                  child: ListTile(
                    title: Text(
                      'Sekali',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          child: Column(
            children: <Widget>[
              ListTile(
                title: Text(
                  'Email',
                  style: TextStyle(
                    color: Colors.deepOrange,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                subtitle: Text(
                  _email,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
              Divider(),
              ListTile(
                title: Text(
                  'No. Handphone',
                  style: TextStyle(
                    color: Colors.deepOrange,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                subtitle: Text(
                  _nohp,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
              Divider(),
              ListTile(
                title: Text(
                  'Alamat',
                  style: TextStyle(
                    color: Colors.deepOrange,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                subtitle: Text(
                  _alamat,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Profile',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Palette.bg1,
          title: Center(
            child: const Text('Profil'),
          ),
        ),
        body: _login == true ? _profile() : LoginPage(),
      ),
    );
  }
}
