import 'package:flutter/material.dart';
import 'package:toko_online/constants.dart';

class PaymentPage extends StatefulWidget {
  final String subtotal;
  final Widget? child;

  PaymentPage(this.subtotal, {Key? key, this.child}) : super(key: key);
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Payment'),
          backgroundColor: Palette.bg1,
        ),
        body: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: double.infinity,
                child: Image.asset('assets/images/qris.jpg'),
              ),
              Padding(
                padding: EdgeInsets.only(left: 0, top: 15.0, bottom: 5.0),
                child: Container(
                  child: Text(
                    'Rp.' + widget.subtotal,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w600,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  width: 400.0,
                  padding: EdgeInsets.only(
                      bottom: 2.0, right: 10.0, top: 2.0, left: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                      topLeft: Radius.circular(10.0),
                      bottomLeft: Radius.circular(10.0),
                    ),
                    color: Palette.colorrekom,
                    boxShadow: [
                      BoxShadow(color: Palette.colorrekom, spreadRadius: 1),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
