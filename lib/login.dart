import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:toko_online/constants.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:toko_online/helper/dbhelper.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController userNameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool visible = false;
  DbHelper dbHelper = DbHelper();

  @override
  void initState() {
    super.initState();
  }

  _updateKeranjang(String userid) async {
    Database db = await dbHelper.database;
    var batch = db.batch();
    db.execute('update keranjang set userid=?', [userid]);
    await batch.commit();
  }

  saveValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('key', "value");
  }

  _cekLogin() async {
    // setState(() {
    //   visible = true;
    // });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var params = "/api/login?email=" +
        userNameController.text +
        "&password=" +
        passwordController.text;

    try {
      var res = await http.get(Uri.parse(Palette.sUrl + params));
      //print(res);
      if (res.statusCode == 200) {
        var response = json.decode(res.body);
        //print(response['response_status']);
        if (response['response_status'] == "OK") {
          //print(response['data'][0]['username']);
          //_updateKeranjang(response['data'][0]['username']);

          prefs.setBool('login', true);
          prefs.setInt('username', response['data'][0]['username']);
          prefs.setString('nama', response['data'][0]['nama']);
          prefs.setString('email', response['data'][0]['email']);
          prefs.setString('foto', response['data'][0]['foto']);
          prefs.setString('nohp', response['data'][0]['nohp']);
          prefs.setString('alamat', response['data'][0]['alamat']);
          //print(response['response_status']);
          // setState(() {
          //   visible = false;
          // });
          Database db = await dbHelper.database;
          var batch = db.batch();
          db.execute('update keranjang set userid=?',
              [response['data'][0]['username']]);
          await batch.commit();
          //if (widget.nav == "") {
          //  Navigator.of(context).pushNamedAndRemoveUntil(
          //    '/landingusers', (Route<dynamic> route) => false);
          //} else {

          Navigator.of(context).pushNamedAndRemoveUntil(
              '/keranjangusers', (Route<dynamic> route) => false);
        } else {
          _showAlertDialog(context, response['response_message']);
        }
      }
    } catch (e) {}
  }

  _showAlertDialog(BuildContext context, String err) {
    Widget okButton = FlatButton(
      child: Text("OK"),
      //onPressed: () {},
      onPressed: () => Navigator.pop(context),
    );
    AlertDialog alert = AlertDialog(
      title: Text("Error"),
      content: Text(err),
      actions: [
        okButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _header(),
                Container(
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                      Visibility(
                          maintainSize: true,
                          maintainAnimation: true,
                          maintainState: true,
                          visible: visible,
                          child: Container(child: CircularProgressIndicator())),
                    ])),
                _body(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _header() {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            alignment: Alignment.topRight,
            padding: EdgeInsets.fromLTRB(0.0, 20.0, 10.0, 0.0),
            child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Icon(Icons.clear, size: 50.0)),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(15.0, 110.0, 0.0, 0.0),
            child: Text('Dicoating',
                style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold)),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(16.0, 145.0, 0.0, 0.0),
            child: Text('sekali',
                style: TextStyle(fontSize: 60.0, fontWeight: FontWeight.bold)),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(170.0, 128.0, 0.0, 20.0),
            child: Text('.',
                style: TextStyle(
                    fontSize: 80.0,
                    fontWeight: FontWeight.bold,
                    color: Palette.menuNiaga)),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(260.0, 108.0, 0.0, 20.0),
            child: Image.asset(
              'assets/images/logo-splash.png',
              width: 100,
              height: 100,
            ),
          ),
        ],
      ),
    );
  }

  Widget _body() {
    return Container(
        padding: EdgeInsets.only(top: 0.0, left: 20.0, right: 20.0),
        child: Column(
          children: <Widget>[
            TextField(
              controller: userNameController,
              keyboardType: TextInputType.text,
              autocorrect: false,
              decoration: InputDecoration(
                  labelText: 'USERNAME',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Palette.menuNiaga))),
            ),
            SizedBox(height: 20.0),
            TextField(
              controller: passwordController,
              keyboardType: TextInputType.text,
              autocorrect: false,
              decoration: InputDecoration(
                  labelText: 'PASSWORD',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Palette.menuNiaga))),
              obscureText: true,
            ),
            SizedBox(height: 5.0),
            Container(
              alignment: Alignment(1.0, 0.0),
              padding: EdgeInsets.only(top: 15.0, left: 20.0),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed('/forgot');
                },
                child: Text(
                  'Lupa Password',
                  style: TextStyle(
                      fontSize: 16.0,
                      color: Palette.menuNiaga,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Montserrat',
                      decoration: TextDecoration.underline),
                ),
              ),
            ),
            SizedBox(height: 40.0),
            InkWell(
              //onTap: _cekLogin(),
              // onTap: () {
              //   Navigator.of(context).pushNamedAndRemoveUntil(
              //       '/dashboard', (Route<dynamic> route) => false);
              // },
              onTap: _cekLogin,
              child: Container(
                height: 60.0,
                child: Material(
                  borderRadius: BorderRadius.circular(10.0),
                  shadowColor: Colors.blue[800],
                  color: Palette.menuNiaga,
                  elevation: 7.0,
                  child: Center(
                    child: Text(
                      'LOGIN',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat'),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 15.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Belum punya akun?',
                  style: TextStyle(fontSize: 16.0, fontFamily: 'Montserrat'),
                ),
                SizedBox(width: 5.0),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pushNamed('/signup');
                  },
                  child: Text(
                    'Daftar Sekarang',
                    style: TextStyle(
                        fontSize: 16.0,
                        color: Palette.menuNiaga,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline),
                  ),
                )
              ],
            ),
            SizedBox(height: 55.0),
          ],
        ));
  }
}
