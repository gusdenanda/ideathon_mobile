import 'package:flutter/material.dart';

class Palette {
  static String sUrl = "http://10.0.2.2:8000";
  //static String sUrl = "http://dicoatingsekali.oss.web.id";
  static String vUrl = "http://172.0.0.1:8000";

  static Color bg1 = Color.fromRGBO(0, 0, 102, 1);
  static Color bg2 = Color.fromRGBO(0, 0, 110, 1);
  static Color orange = Color(0xfff7892b);
  static Color menuNiaga = Colors.blueAccent;
  static Color colorrekom = Color.fromARGB(255, 255, 0, 0);

  static List<Color> colors = <Color>[
    Color.fromARGB(255, 255, 0, 0),
    Color.fromARGB(255, 255, 128, 0),
    Color.fromARGB(255, 0, 178, 0),
  ];
}
